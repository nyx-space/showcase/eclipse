# Eclipse

This is quick example on how to find the minimum and maximum of a Penumbra event for a LEO object subjected to the point masses of the Earth, Moon, and Sun.

[![Gitpod Run on the cloud](https://img.shields.io/badge/Gitpod-Run_on_the_cloud-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/nyx-space/showcase/eclipse)