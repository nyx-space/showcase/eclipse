extern crate nyx_space as nyx;

use nyx::cosmic::eclipse::EclipseLocator;
use nyx::md::ui::*;

fn main() {
    // Load the solar system
    // https://nyx-space.gitlab.io/nyx/nyx_space/cosmic/eclipse/struct.Cosm.html#method.frame
    let cosm = Cosm::de438();

    // Grab the ECI frame (Earth Mean Equator J2000, EME2000)
    // https://nyx-space.gitlab.io/nyx/nyx_space/cosmic/eclipse/struct.Cosm.html#method.frame
    let eci = cosm.frame("EME2000");

    // Choose an epoch
    // https://nyx-space.gitlab.io/nyx/nyx_space/time/struct.Epoch.html#method.from_gregorian_utc_at_midnight
    let epoch = Epoch::from_gregorian_utc_at_midnight(2022, 2, 28);

    // Build an orbit from its altitude, eccentricity, inclination, RAAN, AoP, and true anomaly
    // https://nyx-space.gitlab.io/nyx/nyx_space/struct.Orbit.html#method.keplerian_altitude
    let orbit = Orbit::keplerian_altitude(450.0, 0.001, 63.4, 135.0, 90.0, 60.0, epoch, eci);
    // let orbit = Orbit::cartesian(
    //     -2436.45, -2436.45, 6891.037, 5.088_611, -5.088_611, 0.0, epoch, eci,
    // );

    // Build a point masses propagator
    // https://nyx-space.gitlab.io/nyx/nyx_space/dynamics/orbital/struct.OrbitalDynamics.html#method.point_masses
    let prop = Propagator::dp78(
        OrbitalDynamics::point_masses(&[Bodies::Earth, Bodies::Luna, Bodies::Sun], cosm.clone()),
        PropOpts::with_tolerance(1e-9),
    );

    // Propagate for one orbital period while generating the trajectory
    // https://nyx-space.gitlab.io/nyx/nyx_space/propagators/struct.PropInstance.html#method.for_duration_with_traj
    let (_, traj) = prop
        .with(orbit)
        .for_duration_with_traj(orbit.period())
        .unwrap();

    // Create the searching event
    // MathSpec: https://nyxspace.com/MathSpec/celestial/eclipse/
    let e_loc = EclipseLocator {
        light_source: cosm.frame("Sun J2000"),
        shadow_bodies: vec![eci],
        cosm: cosm.clone(),
    };

    // Build a penumbra event from this eclipse locator
    // https://nyx-space.gitlab.io/nyx/nyx_space/cosmic/eclipse/struct.EclipseLocator.html#method.to_penumbra_event
    let umbra_event_loc = e_loc.to_penumbra_event();

    // Find the minimum and maximum of this penumbra event using a Brent Solver with a one second precision
    // https://nyx-space.gitlab.io/nyx/nyx_space/md/ui/struct.Traj.html#method.find_minmax
    let (min_penumbra, max_penumbra) = traj
        .find_minmax(&umbra_event_loc, TimeUnit::Second)
        .unwrap();

    println!(
        "Min event: {:x} => {}",
        min_penumbra,
        e_loc.compute(&min_penumbra),
    );
    println!(
        "Min event - 1 s: {:x} => {}",
        traj.at(min_penumbra.epoch() - 1_i32.seconds()).unwrap(),
        e_loc.compute(&traj.at(min_penumbra.epoch() - 1_i32.seconds()).unwrap()),
    );
    println!(
        "Min event + 1 s: {:x} => {}",
        traj.at(min_penumbra.epoch() + 1_i32.seconds()).unwrap(),
        e_loc.compute(&traj.at(min_penumbra.epoch() + 1_i32.seconds()).unwrap()),
    );

    println!();

    println!(
        "Max event: {:x} => {}",
        max_penumbra,
        e_loc.compute(&max_penumbra),
    );
    println!(
        "Max event - 1 s: {:x} => {}",
        traj.at(max_penumbra.epoch() - 1_i32.seconds()).unwrap(),
        e_loc.compute(&traj.at(max_penumbra.epoch() - 1_i32.seconds()).unwrap()),
    );
    println!(
        "Max event + 1 s: {:x} => {}",
        traj.at(max_penumbra.epoch() + 1_i32.seconds()).unwrap(),
        e_loc.compute(&traj.at(max_penumbra.epoch() + 1_i32.seconds()).unwrap()),
    );
}
